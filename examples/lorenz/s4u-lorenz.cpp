#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <string>
#include <iostream>
#include <fstream>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

// SYSTEM'S STATE

const double max_sim_time = 100;
const double sampling_time = 0.001;

// UTILITY
std::ofstream out;

static void log_results(){
  double t = simgrid::s4u::Engine::get_clock();
  double x = simgrid::fmi::get_real("lorenz_x","x");
  double y = simgrid::fmi::get_real("lorenz_y","y");
  double z = simgrid::fmi::get_real("lorenz_z","z");
  out << t << ";" << x << ";" << y << ";" << z << "\n";
}

static void sampler(){
  XBT_INFO("perform co-simulation of Lorenz system until time");
  while(simgrid::s4u::Engine::get_clock() < max_sim_time){
    log_results();
    simgrid::s4u::this_actor::sleep_for(sampling_time);
  }
  log_results();
  out.close();
  XBT_INFO("co-simulation of Lorenz system done ! see you !");
}


// MAIN

int main(int argc, char *argv[])
{

  // SIMGRID INIT
  simgrid::s4u::Engine e(&argc, argv);

  const double intstepsize = std::stod(argv[1]);
  simgrid::fmi::init(intstepsize);

  e.load_platform("../../platforms/clusters_rennes.xml");

  // ADDING FMUs

  std::string fmu_uri = "lorenz_x/lorenz_x.fmu";
  std::string fmu_name = "lorenz_x";
  simgrid::fmi::add_fmu_cs(fmu_uri, fmu_name);

  std::string fmu_uri2 = "lorenz_y/lorenz_y.fmu";
  std::string fmu_name2 = "lorenz_y";
  simgrid::fmi::add_fmu_cs(fmu_uri2, fmu_name2);

  std::string fmu_uri3 = "lorenz_z/lorenz_z.fmu";
  std::string fmu_name3 = "lorenz_z";
  simgrid::fmi::add_fmu_cs(fmu_uri3, fmu_name3);

  // CONNECTING FMUS

  simgrid::fmi::connect_fmu("lorenz_x","x","lorenz_y","x");
  simgrid::fmi::connect_fmu("lorenz_x","x","lorenz_z","x");
  simgrid::fmi::connect_fmu("lorenz_y","y","lorenz_x","y");
  simgrid::fmi::connect_fmu("lorenz_y","y","lorenz_z","y");
  simgrid::fmi::connect_fmu("lorenz_z","z","lorenz_y","z");

  // LOG OUTPUT
  out.open("output.csv", std::ios::out);
  out << "\"time\",\"x\",\"y\",\"z\"\n";

  simgrid::fmi::ready_for_simulation();

  // CREATING SIMGRID ACTORS

  simgrid::s4u::Actor::create("waiting_actor", simgrid::s4u::Host::by_name("c-0.rennes"), sampler);

  e.run();
}
