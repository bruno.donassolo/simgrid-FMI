#! /bin/sh

set -e

if [ -e deps/fmi4cpp ] ; then
  cd deps/fmi4cpp ; git pull ; cd ../..
else 
  mkdir -p deps ; git clone --depth=1 https://github.com/NTNU-IHB/FMI4cpp.git deps/fmi4cpp
fi

mkdir -p deps/fmi4cpp/build && cd deps/fmi4cpp/build && cmake -DCMAKE_INSTALL_PREFIX=../install -DBUILD_SHARED_LIBS=ON .. && make install VERBOSE=1 && cd ../..

