#ifndef INCLUDE_MASTER_HPP_
#define INCLUDE_MASTER_HPP_

#include <string>
#include <vector>
#include <unordered_map>
#include <simgrid/kernel/resource/Model.hpp>
#include <fmi4cpp/fmi4cpp.hpp>
#include <boost/functional/hash.hpp>
#include <iostream>
#include <fstream>

struct port{
  std::string fmu;
  std::string name;
};

bool operator== (port a, port b){
  return (a.fmu == b.fmu) && (a.name == b.name);
}

namespace std{
  using boost::hash_value;
  using boost::hash_combine;
  template<> struct hash<port>{
    size_t operator()(const port& k) const{
      std::size_t seed = 0;
      hash_combine(seed,hash_value(k.fmu));
      hash_combine(seed,hash_value(k.name));

      return seed;
    }
  };
}

namespace simgrid{
namespace fmi{

/* Coupling SimGrid -> FMU */
struct real_simgrid_fmu_connection{
  port in;
  double (*generateInput)(std::vector<std::string>);
  std::vector<std::string> params;
};

struct integer_simgrid_fmu_connection{
  port in;
  int (*generateInput)(std::vector<std::string>);
  std::vector<std::string> params;
};

struct boolean_simgrid_fmu_connection{
  port in;
  bool (*generateInput)(std::vector<std::string>);
  std::vector<std::string> params;
};

struct string_simgrid_fmu_connection{
  port in;
  std::string (*generateInput)(std::vector<std::string>);
  std::vector<std::string> params;
};


class MasterFMI : public simgrid::kernel::resource::Model{

private:
  /*
   * The set of FMUs to simulate
   */
  std::unordered_map<std::string, std::unique_ptr<fmi4cpp::fmi2::cs_slave>> fmus;
  /*
   * Indicate if an FMU require an iteration (i.e. doStep(O)) to update its outputs when setting input
   */
  std::unordered_map<std::string,bool> iterate_input;

  /**
   * coupling between FMUs (nb: key=input value=output !)
   */
  std::unordered_map<port,port> couplings;
  std::vector<port> in_coupled_input;

  /**
   * coupling between SimGrid models and FMUs
   */
  std::vector<real_simgrid_fmu_connection> real_ext_couplings;
  std::vector<integer_simgrid_fmu_connection> integer_ext_couplings;
  std::vector<boolean_simgrid_fmu_connection> boolean_ext_couplings;
  std::vector<string_simgrid_fmu_connection> string_ext_couplings;

  std::vector<port> ext_coupled_input;
  std::ofstream output;

  //std::vector<port> monitored_ports;

  bool ready_for_simulation;

  /**
   * last output values send to the input
   */
  std::unordered_map<port,double> last_real_outputs;
  std::unordered_map<port,int> last_int_outputs;
  std::unordered_map<port,bool> last_bool_outputs;
  std::unordered_map<port,std::string> last_string_outputs;

  double nextEvent;
  double commStep;
  double current_time;

  bool firstEvent;
  bool externalCoupling;

  std::vector<void (*)(std::vector<std::string>)> event_handlers;
  std::vector<bool (*)(std::vector<std::string>)> event_conditions;
  std::vector<std::vector<std::string>> event_params;

  bool check_event_occurence();
  void manage_event_notification();
  void solve_couplings(bool firstIteration);
  bool solve_coupling(port in, port out, bool checkChange);
  void solve_external_couplings();
  void check_port_validity(std::string fmu_name, std::string port_name, std::string type, bool check_already_coupled);
  bool is_input_coupled(std::string fmu, std::string input_name);
  void check_not_ready_for_simulation();


public:
  explicit MasterFMI(const double stepSize);
  ~MasterFMI();
  void add_fmu_cs(std::string fmu_uri, std::string fmu_name, bool iterateAfterInput);
  void update_actions_state(double now, double delta) override;
  
  double get_real(std::string fmi_name, std::string output_name, bool checkPort=false);
  bool get_boolean(std::string fmi_name, std::string output_name, bool checkPort=false);
  int get_integer(std::string fmi_name, std::string output_name, bool checkPort=false);
  std::string get_string(std::string fmi_name, std::string output_name, bool checkPort=false);
  var_type get_var_type(std::string fmu_name, std::string var_name);

  void set_real(std::string fmi_name, std::string input_name, double value, bool simgrid_input);
  void set_boolean(std::string fmi_name, std::string input_name, bool value, bool simgrid_input);
  void set_integer(std::string fmi_name, std::string input_name, int value, bool simgrid_input);
  void set_string(std::string fmi_name, std::string input_name, std::string value, bool simgrid_input);
  
  double next_occurring_event(double now) override;
  void register_event(bool (*condition)(std::vector<std::string>), void (*handleEvent)(std::vector<std::string>), std::vector<std::string> params);
  void delete_events();
  void connect_fmu(std::string out_fmu_name,std::string output_port,std::string in_fmu_name,std::string input_port);
  
  void connect_real_to_simgrid(double (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name);
  void connect_integer_to_simgrid(int (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name);
  void connect_boolean_to_simgrid(bool (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name);
  void connect_string_to_simgrid(std::string (*generateInput)(std::vector<std::string>), std::vector<std::string> params, std::string fmu_name, std::string input_name);
  
  void init_couplings();
};

}
}

#endif /* INCLUDE_MASTER_HPP_ */
