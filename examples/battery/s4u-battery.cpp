#include "simgrid/plugins/energy.h"
#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include "simgrid/s4u/VirtualMachine.hpp"
#include "simgrid/plugins/live_migration.h"
#include <string>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this s4u example");

// PARAMETERS
int nb_hosts_per_cluster = 129;
double max_sim_time = 400;

// precision of the cluster consumption test (in Wh)
double wh_precision = 0.01;

double battery_capacity = 8000000;

// FMU INPUT FROM SIMGRID MODEL
static double getDCPowerConsumption(std::vector<std::string>){
  double total_power = 0;

  for(int i=0;i<nb_hosts_per_cluster;i++){
    std::string host_name = "c-" + std::to_string(i) + ".rennes";
    simgrid::s4u::Host* host = simgrid::s4u::Host::by_name(host_name);
    total_power += sg_host_get_current_consumption(host);
  }
  return total_power;
}

// EVENT DETECTORS
static bool detectPowerShutDown(std::vector<std::string>){
  return simgrid::fmi::get_real("battery","Pout") <= 0 && simgrid::s4u::Engine::get_clock() > 0 ;
}

// EVENT CALLBACKS
static void checkResult(){

  double cluster_consumption = 0;

  for(int i=0;i<nb_hosts_per_cluster;i++){
    std::string host_name = "c-" + std::to_string(i) + ".rennes";
    simgrid::s4u::Host* host = simgrid::s4u::Host::by_name(host_name);
    cluster_consumption += sg_host_get_consumed_energy(host);
  }

  XBT_INFO("consumption of the Rennes cluster = %f J", cluster_consumption);
  double j_precision = wh_precision * 3600;
  assert(cluster_consumption <= battery_capacity + j_precision && cluster_consumption >= battery_capacity - j_precision);
  XBT_INFO("As expected, the Rennes cluster consumption (%f J) is equivalent to the capacity of the battery (%f J)", cluster_consumption, battery_capacity);
}

static void shutDownRennesHosts(std::vector<std::string>){

  double power_supply = simgrid::fmi::get_real("battery","Pout");
  
  for(int i=0;i<nb_hosts_per_cluster;i++){
    std::string host_name = "c-" + std::to_string(i) + ".rennes";
    simgrid::s4u::Host* host = simgrid::s4u::Host::by_name(host_name);
    if(host->is_on()){
      for(simgrid::s4u::ActorPtr actor : host->get_all_actors()){
        actor->kill();
      }
      host->turn_off();
    }
  }

  XBT_INFO("all the PM of Rennes are shutdown because the battery does not supply enough power (%f W)",power_supply);
  checkResult();
}

// ACTORS' BEHAVIOR
static int task_runner(std::vector<std::string> args){
  
  double slave_flop_works = std::stod(args[0]);
  XBT_INFO("running a tasks of %f flops",slave_flop_works);
  simgrid::s4u::this_actor::execute(slave_flop_works);
  XBT_INFO("task %s done, see you",simgrid::s4u::this_actor::get_name().c_str());
  
  return 0;  
}

static int deploy_task_runners(){

  int nb_core = simgrid::s4u::Host::by_name("c-0.rennes")->get_core_count();
  double deadline_gap = max_sim_time/ (nb_core * nb_hosts_per_cluster);
  double task_deadline = deadline_gap;

  for(int i=0;i<nb_hosts_per_cluster;i++){
    std::string host_name = "c-" + std::to_string(i) + ".rennes";
    simgrid::s4u::Host* host = simgrid::s4u::Host::by_name(host_name);
    
    double pm_speed = host->get_speed();
    
    for(int j=0;j<nb_core;j++){
      double flop = task_deadline * pm_speed;
      task_deadline += deadline_gap;
      std::string actor_name = "task_runner_"+std::to_string(i*j);
      std::vector<std::string> actor_args = {std::to_string(flop)};
      simgrid::s4u::Actor::create(actor_name, host, task_runner, actor_args);
    }
  }

  return 0;
}

// MAIN

int main(int argc, char *argv[])
{
  // SIMGRID INIT
  sg_host_energy_plugin_init();
  simgrid::s4u::Engine e(&argc, argv);

  const double intstepsize = 0.01;
  simgrid::fmi::init(intstepsize);

  e.load_platform("../../platforms/clusters_rennes.xml");

  // ADDING FMUs
  std::string fmu_uri = (argc>=2)? argv[1] : "battery_fmu/battery.fmu";
  std::string fmu_name = "battery";
  simgrid::fmi::add_fmu_cs(fmu_uri, fmu_name);

  simgrid::fmi::connect_real_to_simgrid(getDCPowerConsumption,{},"battery","Pneed");
 
  simgrid::fmi::ready_for_simulation();

  // CREATING SIMGRID ACTORS
  deploy_task_runners();
 
  // REGISTERING EVENT
  simgrid::fmi::register_event(detectPowerShutDown, shutDownRennesHosts,{});

  e.run();
}
