#!/bin/bash

cd examples/lotka-volterra

# we run the FMU co-simulation of the Lotka-Volterra system with SimGrid (with the FMU communication step as a parameter)
x="$(find `pwd` -name lotka_volterra_x.fmu)"
y="$(find `pwd` -name lotka_volterra_y.fmu)"

echo "$x $y"

./s4u-lotka-volterra "$x" "$y" 0.001

res="$?"

if [ "$res" -ne "0" ]
then
        echo "error: the SimGrid simulation did not end properly"
        exit 1
fi

# we run the monolithic simulation of the Lotka-Volterra system with OpenModelica
omc runMonolithicLotkaVolterra.mos 

res="$?"

if [ "$res" -ne "0" ]
then
        echo "error: the OpenModelica simulation did not end properly"
        exit 1
fi


# we compare the output logs (i.e. the computed solutions of the Lotka-Volterra system)
awk -F'[;,]' 'function abs(v){return v<0 ? -v : v} NR==FNR{x[FNR]=$2; y[FNR]=$3; next}; (abs(x[FNR]-$2) > 0.01 || abs(y[FNR]-$3) > 0.01 ){exit 1}' monolithic_lotka_volterra/monolithic_lotka_volterra_res.csv output.csv

res="$?"

if [ "$res" -ne "0" ]
then
	echo "error: the gap between SimGrid co-simulation and OpenModelica monolithic simulation is too big"
	exit 1
fi

exit 0

