This project is [hosted on framagit](https://framagit.org/simgrid/simgrid-FMI), and sometimes mirrored to GitHub.
Please use our [framagit project](https://framagit.org/simgrid/simgrid-FMI).

# SimGrid-FMI
[SimGrid](https://simgrid.org/) plugin based on the [FMI standard](https://fmi-standard.org/) to co-simulate distributed IT infrastructures and continuous multi-physical systems.

# Installation
Works on Linux and Windows (with Windows Subsystem Linux).

First, you need to install [SimGrid](https://simgrid.org/) and [FMI4cpp](https://github.com/NTNU-IHB/FMI4cpp).

Then, you can build and install SimGrid-FMI:
```
cmake .
# Or cmake -DSimGrid_PATH=/path/to/simgrid -DFMI4cpp_PATH=/path/to/fmi4cpp . if CMake can not find SimGrid and/or FMI4cpp.
make
make install # try "sudo make install" if you don't have the permission to write
```

To find the SimGrid-FMI library for your own project, you can use as a CMake module the [FindSimGridFMI.cmake](https://framagit.org/simgrid/simgrid-FMI/blob/master/FindSimGridFMI.cmake) file that is located at the root of the SimGrid-FMI tree.

This will define a target called SimGridFMI::SimgridFMI. Use it as:
```
target_link_libraries(your-simulator SimGridFMI::SimGridFMI)
```

# Testing your build
```
# Run and test several examples that use FMUs exported with OpenModelica.
# First, you need to install OpenModelica and export all the FMUs.
for deb in deb deb-src; do echo "$deb http://build.openmodelica.org/apt `lsb_release -cs` nightly"; done | tee /etc/apt/sources.list.d/openmodelica.list
wget -q http://build.openmodelica.org/apt/openmodelica.asc -O- | apt-key add -
apt update
apt install -y openmodelica
./examples/buildFMU.sh
# Then, you can run the tests.
ctest
# If you want to run the official FMI standard cross-check
cd tests
git clone https://github.com/modelica/fmi-cross-check.git fmi-cross-check
python3 -m pip install fmpy[complete] numpy scipy
./test_fmus.sh
```

# Publications
* [Co-simulation of an electrical distribution network and its supervision communication network](https://hal.archives-ouvertes.fr/hal-02352832)  
Benjamin Camus, Anne Blavette, Anne-Cécile Orgerie, Jean-Baptiste Blanc-Rouchossé.  
IEEE CCNC 2020.

* [Co-simulation of FMUs and distributed applications with SimGrid](https://hal.inria.fr/hal-01762540)  
Benjamin Camus, Anne-Cécile Orgerie, Martin Quinson.  
ACM SIGSIM-PADS 2018.
