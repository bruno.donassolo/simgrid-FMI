#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <limits>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

/**
* In this example, we consider a simple FMU with an output variable x=sin(time).
* We test that we can properly detect and trigger event when x cross the x = 0 threshold.
*/

std::string input_file_suffix = "_in.csv";
std::string ref_file_suffix = "_ref.csv";
std::string output_file_suffix = "_out.csv";
const char delim = ',';
std::string delims = ",\"\n\r";

std::string fmu_name;
std::string fmu_path;
const double step_size = 99999999999;

double next_input_time = 0;
double next_output_time = 0;
bool input_ready = true;

std::vector<std::string> output_ids;
std::ofstream out;


/**
 * Utility
 */
static void get_vars(std::string first_line, std::vector<std::string> &var_ids){
  std::vector<std::string> ids;
  boost::split(ids, first_line, boost::is_any_of(delims));
  for(std::string id : ids)
    if(!id.empty()) var_ids.push_back(id);
}

static void get_var_values(std::string line, std::vector<double> &var_values){
  std::stringstream ss(line);
  std::string token;
  while (std::getline(ss, token, delim))
    var_values.push_back(std::stod(token));
}


static void print_output(){
  out << simgrid::s4u::Engine::get_clock();

  for(int i=1;i<output_ids.size();i++){
    simgrid::fmi::var_type type = simgrid::fmi::get_var_type(fmu_name, output_ids[i]); 
    if(type == simgrid::fmi::var_type::real){
      double value = simgrid::fmi::get_real(fmu_name, output_ids[i]);
      XBT_INFO("%s = %f", output_ids[i].c_str(), value);
      out << delim << value;
    }else if(type == simgrid::fmi::var_type::integer){
      int value = simgrid::fmi::get_integer(fmu_name, output_ids[i]);
      XBT_INFO("%s = %i", output_ids[i].c_str(), value);
      out << delim << value;
    }else if(type == simgrid::fmi::var_type::boolean){
      bool value = simgrid::fmi::get_boolean(fmu_name, output_ids[i]);
      XBT_INFO("%s = %i", output_ids[i].c_str(), value);
      out << delim << value;
    }else if(type == simgrid::fmi::var_type::string){
      std::string value = simgrid::fmi::get_string(fmu_name, output_ids[i]);
      XBT_INFO("%s = %s", output_ids[i].c_str(), value.c_str());
      out << delim << value;
    }
  }

  out << "\n";
}

/**
 * Actor behaviors
 */
static void input_manager(){

  XBT_INFO("start managing input for FMU %s", fmu_name.c_str());

  std::ifstream infile(fmu_path + fmu_name + input_file_suffix);
  std::string line;
  std::getline(infile, line);

  std::vector<std::string> var_ids;
  get_vars(line, var_ids);

  while(std::getline(infile, line)){

    std::vector<std::string> var_values;
    get_vars(line, var_values);

    next_input_time = std::stod(var_values[0]);
    simgrid::s4u::this_actor::sleep_until(next_input_time);

    while(next_output_time == next_input_time && !input_ready)
      simgrid::s4u::this_actor::yield();
    
    for(int i=1;i<var_values.size();i++){

      simgrid::fmi::var_type type = simgrid::fmi::get_var_type(fmu_name, var_ids[i]); 
      if(type == simgrid::fmi::var_type::real){
	double value = std::stod(var_values[i]);
	XBT_INFO("set real var %s to %s", var_ids[i].c_str(), var_values[i].c_str());
        simgrid::fmi::set_real(fmu_name, var_ids[i], value);
      }else if(type == simgrid::fmi::var_type::integer){
	int value = std::stoi(var_values[i]);
	XBT_INFO("set integer var %s to %s", var_ids[i].c_str(), var_values[i].c_str());
        simgrid::fmi::set_integer(fmu_name, var_ids[i], value);
      }else if(type == simgrid::fmi::var_type::boolean){
	bool value = std::stoi(var_values[i]);
	if(value){
	  XBT_INFO("set boolean var %s to true (%s)", var_ids[i].c_str(), var_values[i].c_str());
	}else{
          XBT_INFO("set boolean var %s to false (%s)", var_ids[i].c_str(), var_values[i].c_str());
        }
        simgrid::fmi::set_boolean(fmu_name, var_ids[i], value);
      }else if(type == simgrid::fmi::var_type::string){
	XBT_INFO("set string var %s to %s", var_ids[i].c_str(), var_values[i].c_str());
        simgrid::fmi::set_string(fmu_name, var_ids[i], var_values[i]);
      }
    }

    print_output();
    input_ready = false;
  }
 
  next_input_time = std::numeric_limits<double>::max();
  XBT_INFO("end of input for FMU %s", fmu_name.c_str());
}


static void output_manager(){
  XBT_INFO("start managing output of FMU %s", fmu_name.c_str());

  std::ifstream infile(fmu_path + fmu_name + ref_file_suffix);
  std::string line;
  std::getline(infile, line);

  get_vars(line, output_ids);

  out.open(fmu_name + output_file_suffix);
  out << line << "\n";

  while(std::getline(infile, line)){
    
    std::vector<std::string> var_values;
    get_vars(line, var_values);

    next_output_time = std::stod(var_values[0]);
    simgrid::s4u::this_actor::sleep_until(next_output_time);

    while(next_input_time == next_output_time && input_ready)
      simgrid::s4u::this_actor::yield();

    print_output();

    if(next_input_time == next_output_time)
      input_ready = true;
  }

  out.close();
  next_output_time = std::numeric_limits<double>::max();
  XBT_INFO("end of output for FMU %s", fmu_name.c_str());
}


int main(int argc, char *argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  simgrid::fmi::init(step_size);

  e.load_platform("../platforms/clusters_rennes.xml");
  
  simgrid::s4u::Actor::create("input_manager", e.get_all_hosts()[0], input_manager);
  simgrid::s4u::Actor::create("output_manager", e.get_all_hosts()[0], output_manager);

  fmu_path = argv[1];
  fmu_name = argv[2];
  fmu_path += "/";

  simgrid::fmi::add_fmu_cs(fmu_path+fmu_name+".fmu", fmu_name, false);
  simgrid::fmi::ready_for_simulation();

  e.run();

  return 0;
}
