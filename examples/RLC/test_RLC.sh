#!/bin/bash

cd examples/RLC

# we sample the FMU of the RLC system with SimGrid
rlc="$(find `pwd` -name RLC.fmu)"
./s4u-RLC "$rlc"

res="$?"

if [ "$res" -ne "0" ]
then
        echo "error: the SimGrid simulation did not end properly"
        exit 1
fi


# we run the monolithic simulation of the Lotka-Volterra system with OpenModelica
omc runRLC.mos 

res="$?"

if [ "$res" -ne "0" ]
then
        echo "error: the OpenModelica simulation did not end properly"
        exit 1
fi


# we compare the output logs (i.e. the computed solutions of the Lotka-Volterra system)
awk -F'[;,]' 'function abs(v){return v<0 ? -v : v} NR==FNR{a[FNR]=$2; b[FNR]=$3; c[FNR]=$4; d[FNR]=$5; next}; (abs(a[FNR]-$2) > 0.001 || abs(b[FNR]-$3) > 0.001 || abs(c[FNR]-$4) > 0.001 || abs(d[FNR]-$5) > 0.001){printf(FNR " " abs($2-a[FNR]) " " abs($3-b[FNR]) " " abs($4-c[FNR]) " " abs($5-d[FNR])); exit 1}' output.csv openModelica_RLC/RLC_res.csv

res="$?"

if [ "$res" -ne "0" ]
then
	echo "error: the gap between SimGrid co-simulation and OpenModelica monolithic simulation is too big"
	exit 1
fi

exit 0
